//
//  DetectionExperiment.h
//  DetectionExperiment
//
//  Created by Phillip Harris on 4/4/20.
//  Copyright © 2020 Align Technology, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for DetectionExperiment.
FOUNDATION_EXPORT double DetectionExperimentVersionNumber;

//! Project version string for DetectionExperiment.
FOUNDATION_EXPORT const unsigned char DetectionExperimentVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <DetectionExperiment/PublicHeader.h>


