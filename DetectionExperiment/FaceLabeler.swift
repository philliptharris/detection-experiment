//
//  FaceLabeler.swift
//  DetectionExperiment
//
//  Created by Phillip Harris on 4/4/20.
//  Copyright © 2020 Align Technology, Inc. All rights reserved.
//

import Foundation
import Firebase

public class FaceLabeler {
    
    static var isConfigured = false
    
    static func configure() {
        guard !isConfigured else { return }
        FirebaseApp.configure()
    }
    
    private let labeler: VisionImageLabeler
    
    public init?() {
        
        guard let manifestPath = Bundle(for: type(of: self)).path(forResource: "cap-glasses-hat-lips-teeth", ofType: "json") else { return nil }
        
        let model = AutoMLLocalModel(manifestPath: manifestPath)
        
        let options = VisionOnDeviceAutoMLImageLabelerOptions(localModel: model)
        options.confidenceThreshold = 0
        
        labeler = Vision.vision().onDeviceAutoMLImageLabeler(options: options)
    }
    
    public struct Labels {
        
        /// The model's confidence that there are teeth in the image. Range is [0, 1].
        public let teeth: Float?
        
        /// The model's confidence that there are glasses in the image. Range is [0, 1].
        public let glasses: Float?
        
        /// The model's confidence that there is a cap in the image. Range is [0, 1].
        public let cap: Float?
        
        /// The model's confidence that there is a hat in the image. Range is [0, 1].
        public let hat: Float?
        
        /// The model's confidence that there are lips in the image. Range is [0, 1].
        public let lips: Float?
        
        /// Returns a Boolean value indicating whether or not there are teeth in the image, based on the threshold value.
        public func hasTeeth(confidenceThreshold: Float = 0.5) -> Bool? {
            return teeth.map { $0 > confidenceThreshold }
        }
        
        /// Returns a Boolean value indicating whether or not there are glasses in the image, based on the threshold value.
        public func hasGlasses(confidenceThreshold: Float = 0.5) -> Bool? {
            return glasses.map { $0 > confidenceThreshold }
        }
        
        /// Returns a Boolean value indicating whether or not there is a cap in the image, based on the threshold value.
        public func hasCap(confidenceThreshold: Float = 0.5) -> Bool? {
            return cap.map { $0 > confidenceThreshold }
        }
        
        /// Returns a Boolean value indicating whether or not there is a hat in the image, based on the threshold value.
        public func hasHat(confidenceThreshold: Float = 0.5) -> Bool? {
            return hat.map { $0 > confidenceThreshold }
        }
        
        /// Returns a Boolean value indicating whether or not there are lips in the image, based on the threshold value.
        public func hasLips(confidenceThreshold: Float = 0.5) -> Bool? {
            return lips.map { $0 > confidenceThreshold }
        }
    }
    
    public func label(image: UIImage, completion: @escaping (_ result: Result<Labels, Error>) -> Void) {
        
        let visionImage = VisionImage(image: image)
                
        labeler.process(visionImage) { (labels: [VisionImageLabel]?, error: Error?) in
            
            if let error = error {
                completion(.failure(error))
                return
            }
            
            completion(.success(FaceLabeler.makeLabels(from: labels)))
        }
    }
    
    public func label(buffer: CMSampleBuffer, orientation: CGImagePropertyOrientation, completion: @escaping (_ result: Result<Labels, Error>) -> Void) {
        
        let visionImage = VisionImage(buffer: buffer)
        
        let metadata = VisionImageMetadata()
        metadata.orientation = FaceLabeler.visionOrientation(from: orientation)
        visionImage.metadata = metadata
                
        labeler.process(visionImage) { (labels: [VisionImageLabel]?, error: Error?) in
            
            if let error = error {
                completion(.failure(error))
                return
            }
            
            completion(.success(FaceLabeler.makeLabels(from: labels)))
        }
    }
    
    private static func makeLabels(from labels: [VisionImageLabel]?) -> Labels {
        
        var teeth: Float?
        var glasses: Float?
        var cap: Float?
        var hat: Float?
        var lips: Float?
        
        labels?.forEach {
            let floatValue = $0.confidence?.floatValue
            switch $0.text {
            case "teeth": teeth = floatValue
            case "glasses": glasses = floatValue
            case "cap": cap = floatValue
            case "hat": hat = floatValue
            case "lips": lips = floatValue
            default: break
            }
        }
        
        return Labels(teeth: teeth, glasses: glasses, cap: cap, hat: hat, lips: lips)
    }
    
    private static func visionOrientation(from exifOrientation: CGImagePropertyOrientation) -> VisionDetectorImageOrientation {
        switch exifOrientation {
        case .up: return .topLeft
        case .upMirrored: return .topRight
        case .down: return .bottomRight
        case .downMirrored: return .bottomLeft
        case .leftMirrored: return .leftTop
        case .right: return .rightTop
        case .rightMirrored: return .rightBottom
        case .left: return .leftBottom
        }
    }
    
    private static func visionOrientation(from imageOrientation: UIImage.Orientation) -> VisionDetectorImageOrientation {
        switch imageOrientation {
        case .up: return .topLeft
        case .down: return .bottomRight
        case .left: return .leftBottom
        case .right: return .rightTop
        case .upMirrored: return .topRight
        case .downMirrored: return .bottomLeft
        case .leftMirrored: return .leftTop
        case .rightMirrored: return .rightBottom
        @unknown default: return .topLeft
        }
    }
}
