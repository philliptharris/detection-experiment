//
//  UIImage+Cropped.swift
//  DetectionExperimentTests
//
//  Created by Phillip Harris on 4/7/20.
//  Copyright © 2020 Align Technology, Inc. All rights reserved.
//

import UIKit

extension UIImage {
    
    func cropped(toRectangle cropRect: CGRect) -> UIImage? {
        
        return UIImage(cgImage: cgImage!.cropping(to: cropRect)!)
    }
}
