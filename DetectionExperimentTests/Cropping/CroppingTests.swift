//
//  CroppingTests.swift
//  DetectionExperimentTests
//
//  Created by Phillip Harris on 4/7/20.
//  Copyright © 2020 Align Technology, Inc. All rights reserved.
//

import XCTest
@testable import DetectionExperiment

class CroppingTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        FaceLabeler.configure()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testImageCropping() {
        
        guard let detector = FaceLabeler() else {
            XCTFail()
            return
        }
        
        let url = Bundle(for: type(of: self)).url(forResource: "face-for-rotation", withExtension: "jpg")
        let data = try! Data(contentsOf: url!)
        let image = UIImage(data: data)!
        
        print("size = \(image.size) | scale = \(image.scale)")
        
        stride(from: 0.2, through: 1.0, by: 0.1).forEach {
            
            let e = expectation(description: "")
            print("\n\n")
            print($0)
            
            let width = image.size.width * CGFloat($0)
            let aspectRatio: CGFloat = 3 / 4
            let height = width / aspectRatio
            
            let cropRect = CGRect(x: (image.size.width - width) / 2, y: (image.size.height - height) / 2, width: width, height: height)
            
            let croppedImage = image.cropped(toRectangle: cropRect)
            
            print("size = \(croppedImage!.size) | scale = \(croppedImage!.scale)")
            
            detector.label(image: croppedImage!) {
                print($0)
                e.fulfill()
            }
            waitForExpectations(timeout: 60)
        }
    }
}

// Note that cropping DOES impact results, and we might need to crop down to the area of the face. If the face is too small within the image, teeth are not detected. If the face is too big within the image, it looks like teeth are still detected.
