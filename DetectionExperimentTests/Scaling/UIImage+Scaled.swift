//
//  UIImage+Scaled.swift
//  DetectionExperimentTests
//
//  Created by Phillip Harris on 4/6/20.
//  Copyright © 2020 Align Technology, Inc. All rights reserved.
//

import UIKit

extension UIImage {
    
    func scaled(byFactor factor: CGFloat) -> UIImage? {
        
        let scaledSize = CGSize(width: size.width * factor, height: size.height * factor)
        
        let format = UIGraphicsImageRendererFormat.default()
        format.scale = scale
        
        let renderer = UIGraphicsImageRenderer(size: scaledSize, format: format)
        
        return renderer.image { context in
            draw(in: CGRect(origin: .zero, size: scaledSize))
        }
    }
}
