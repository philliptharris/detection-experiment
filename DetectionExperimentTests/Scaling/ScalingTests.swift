//
//  ScalingTests.swift
//  DetectionExperimentTests
//
//  Created by Phillip Harris on 4/6/20.
//  Copyright © 2020 Align Technology, Inc. All rights reserved.
//

import XCTest
@testable import DetectionExperiment

class ScalingTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        FaceLabeler.configure()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testImageScaling() {
        
        guard let detector = FaceLabeler() else {
            XCTFail()
            return
        }
        
        let url = Bundle(for: type(of: self)).url(forResource: "5", withExtension: "jpg") // 24
        let data = try! Data(contentsOf: url!)
        let image = UIImage(data: data)
        
        print("size = \(image!.size) | scale = \(image!.scale)")
        
        stride(from: 0.1, through: 2.0, by: 0.1).forEach {
            
            let e = expectation(description: "")
            print("\n\n")
            print($0)
            
            let scaledImage = image!.scaled(byFactor: CGFloat($0))
            
            print("size = \(scaledImage!.size) | scale = \(scaledImage!.scale)")
            
            detector.label(image: scaledImage!) {
                print($0)
                e.fulfill()
            }
            waitForExpectations(timeout: 60)
        }
    }
}

// Note that image scaling does not seem to impact results. It would be interesting to see if scaling the image as a first step could improve performance (net).
