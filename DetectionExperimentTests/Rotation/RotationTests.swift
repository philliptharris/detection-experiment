//
//  RotationTests.swift
//  DetectionExperimentTests
//
//  Created by Phillip Harris on 4/6/20.
//  Copyright © 2020 Align Technology, Inc. All rights reserved.
//

import XCTest
@testable import DetectionExperiment

class RotationTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        FaceLabeler.configure()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testImageRotation() {
        
        guard let detector = FaceLabeler() else {
            XCTFail()
            return
        }
        
        let url = Bundle(for: type(of: self)).url(forResource: "24", withExtension: "jpg") // face-for-rotation
        let data = try! Data(contentsOf: url!)
        let image = UIImage(data: data)
        
        print("size = \(image!.size) | scale = \(image!.scale)")
        
        stride(from: 0, through: 90, by: 5).forEach {
            
            let e = expectation(description: "")
            print("\n\n")
            print($0)
            
            let rotatedImage = image!.rotated(byDegrees: CGFloat($0))
            
            let cg = rotatedImage?.cgImage!
            
            print("size = \(rotatedImage!.size) | scale = \(rotatedImage!.scale)")
            
            detector.label(image: rotatedImage!) {
                print($0)
                e.fulfill()
            }
            waitForExpectations(timeout: 60)
        }
    }
}

// This is giving some weird results. Something fishy might be going on. Maybe it has something to do with the cropping? Maybe the crop is too tight?
