//
//  UIImage+Rotated.swift
//  DetectionExperimentTests
//
//  Created by Phillip Harris on 4/6/20.
//  Copyright © 2020 Align Technology, Inc. All rights reserved.
//

import UIKit

extension UIImage {
    
    func rotated(byDegrees degrees: CGFloat) -> UIImage? {
        
        let longSide = max(size.width, size.height)
        
        let contextSize = CGSize(width: longSide, height: longSide)
        
        let drawRect = CGRect(origin: CGPoint(x: (longSide - size.width) / 2, y: (longSide - size.height) / 2), size: size)
        
        var transform = CGAffineTransform.identity
        transform = transform.translatedBy(x: longSide / +2, y: longSide / +2)
        transform = transform.rotated(by: degrees * CGFloat.pi / 180)
        transform = transform.translatedBy(x: longSide / -2, y: longSide / -2)
        
        let format = UIGraphicsImageRendererFormat.default()
        format.scale = scale
        
        let renderer = UIGraphicsImageRenderer(size: contextSize, format: format)
        
        let rotatedImage = renderer.image { context in
            context.cgContext.concatenate(transform)
            draw(in: drawRect)
        }
        
//        return rotatedImage
        
        let shortSide = min(size.width, size.height)

        let side = shortSide / CGFloat(2).squareRoot()

        let croppingRect = CGRect(x: (longSide - side) / 2, y: (longSide - side) / 2, width: side, height: side)

        return UIImage(cgImage: rotatedImage.cgImage!.cropping(to: croppingRect)!)
    }
}
