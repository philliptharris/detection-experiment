//
//  DetectionExperimentTests.swift
//  DetectionExperimentTests
//
//  Created by Phillip Harris on 4/4/20.
//  Copyright © 2020 Align Technology, Inc. All rights reserved.
//

import XCTest
@testable import DetectionExperiment

class DetectionExperimentTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        FaceLabeler.configure()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        
        guard let detector = FaceLabeler() else {
            XCTFail()
            return
        }
        
//        for i in 1...24 {
//            let url = Bundle(for: type(of: self)).url(forResource: "\(i)", withExtension: "jpg")
//            let data = try! Data(contentsOf: url!)
//            let image = UIImage(data: data)
//            let e = expectation(description: "")
//            print("\n\n")
//            print("\(i)")
//            detector.label(image: image!) {
//                print($0)
//                e.fulfill()
//            }
//            waitForExpectations(timeout: 60)
//        }
        
        stride(from: 0, through: 315, by: 45).forEach {
            let url = Bundle(for: type(of: self)).url(forResource: "face-\($0)", withExtension: "jpg")
            let data = try! Data(contentsOf: url!)
            let image = UIImage(data: data)
            let e = expectation(description: "")
            print("\n\n")
            print($0)
            detector.label(image: image!) {
                print($0)
                e.fulfill()
            }
            waitForExpectations(timeout: 60)
        }
        
//        let url = Bundle(for: type(of: self)).url(forResource: "should-be-the-same", withExtension: "jpg")
//        let data = try! Data(contentsOf: url!)
//        let image = UIImage(data: data)
//        let e = expectation(description: "")
//        detector.label(image: image!) {
//            print("\n\n")
//            print($0)
//            e.fulfill()
//        }
//        waitForExpectations(timeout: 60)
    }
    
    // Things that could impact any result:
    // Exif orientation
    // Rotation of the face within the frame
    // Pixel dimensions
    // Position of the face within the frame
    // Size of the face within the frame
    
    // Things that might impact glasses detection:
    // The presence of cheek retractors
    
    // Things that might impact hat/cap detection:
    // Hairstyle
}
