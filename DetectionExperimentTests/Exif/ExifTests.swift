//
//  ExifTests.swift
//  DetectionExperimentTests
//
//  Created by Phillip Harris on 4/7/20.
//  Copyright © 2020 Align Technology, Inc. All rights reserved.
//

import XCTest
@testable import DetectionExperiment

class ExifTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        FaceLabeler.configure()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExifOrientation() {
        
        guard let detector = FaceLabeler() else {
            XCTFail()
            return
        }
        
        (1...8).forEach {
            let url = Bundle(for: type(of: self)).url(forResource: "exif-\($0)", withExtension: "jpg")
            let data = try! Data(contentsOf: url!)
            let image = UIImage(data: data)
            let e = expectation(description: "")
            print("\n\n")
            print($0)
            detector.label(image: image!) {
                print($0)
                e.fulfill()
            }
            waitForExpectations(timeout: 60)
        }
    }
}
