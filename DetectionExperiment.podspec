Pod::Spec.new do |s|
  s.name = "DetectionExperiment"
  s.version = "0.0.1"
  s.summary = "An experiment."
  s.description = "This is an experiment I am running."
  s.homepage = "https://www.ncsu.edu"
  s.license = "Copyright © 2020 Align Technology, Inc. All rights reserved."
  s.author = { "Phillip Harris" => "pharris@aligntech.com" }
  s.source = { :git => "https://gitlab.com/philliptharris/detection-experiment.git", :tag => s.version }
  s.platform = :ios
  s.ios.deployment_target = "11.0"
  s.swift_version = "5.1"
  s.source_files = "DetectionExperiment/*.swift"
  s.resource = "DetectionExperiment/TensorFlowLiteModel/*"
  s.dependency "Firebase/MLVisionAutoML"
  s.static_framework = true
end
